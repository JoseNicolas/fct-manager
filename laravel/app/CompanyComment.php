<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyComment extends Model
{
    protected $table = "company_ comments";

    public function user(){

        return $this->hasOne('App\User');
    }
}
