<?php

// Home
Breadcrumbs::register('/', function($breadcrumbs)
{
    $breadcrumbs->push('Home', url('/'));
});

Breadcrumbs::register('users.index', function($breadcrumbs) {
    $breadcrumbs->parent('/');
    $breadcrumbs->push('Usuarios', route('users.index'));
});

Breadcrumbs::register('users.edit', function($breadcrumbs) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('Editar Usuario', url('users/{$id}/edit'));
});

Breadcrumbs::register('users.create', function($breadcrumbs) {
    $breadcrumbs->parent('users.index');
    $breadcrumbs->push('Crear Usuario', url('users/create'));
});