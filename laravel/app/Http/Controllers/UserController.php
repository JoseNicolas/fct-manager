<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Role;


class UserController extends Controller
{

    //Middleware de autentificacion
    public function __construct()
    {
        $this->middleware('auth');
    }


    /***************************************************
     *                  METODOS CRUD
     **************************************************/

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$users = User::all();
        $users = User::with('Role')->get();
//        $users = User::with('Role')->where('active', '=', 1)->get();

        //$var = User::with('role');

        return view('users.users_index', ["users" => $users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('users.users_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {

        $user = new User;

        $user->dni = $request->dni;
        $user->name = $request->name;
        $user->surnames = $request->surnames;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->province = $request->province;
        $user->locality = $request->locality;
        $user->cp = $request->cp;
        $user->email = $request->email;
        $user->gender = $request->geder;
        $user->username= $request->username;
        $user->password = $request->password;
        //$user->password = bcrypt($request->password);
        $user->role_id = 1;
        $user->active = 0;

        $user->save();

        return redirect()->action('UserController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('users.users_edit', ["user" => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);

        $user->dni = $user->dni;
        $user->name = $request->name;
        $user->surnames = $request->surnames;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->province = $request->province;
        $user->locality = $request->locality;
        $user->cp = $request->cp;
//        $user->email = $user->email;
        $user->gender = $user->geder;
        $user->username= $user->username;
        $user->password = $request->password;
        //$user->password = bcrypt($request->password);
//        $user->role_id = $user->role_id;
//        $user->active = $user->active;

        $user->save();

//        return redirect()->action('UserController@index')
        return redirect('/users')->with('status', 'Perfil Actualizado!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->active = 0;

        $user->save();

        return redirect('users/');
    }
}
