<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PruebasController extends Controller
{
    public function index(Request $request){

        $name = $request->session()->get('name');

        return view('pruebas', ['name' => $name]);
    }
}
