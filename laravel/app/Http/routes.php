<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Pagina index
Route::get('/', 'HomeController@index');
//Pagina dashboard
Route::get('/home', 'HomeController@index');

//Peticiones tipo user
Route::resource('users', 'UserController');

//Peticiones tipo company
Route::resource('companies', 'CompanyController');

Route::auth();


