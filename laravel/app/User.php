<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

class User extends Authenticatable
{

    protected $table = 'users';

    protected $fillable = array(
        'dni',
        'name',
        'surnames',
        'phone',
        'email',
        'address',
        'province',
        'locality',
        'cp',
        'role_id',
        'active',
        'id'
    );

    protected $hidden = ['password'];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /*************
     * Relations
     *************/

    public function role(){

        return $this->belongsTo('App\Role');
    }

    public function companycomment(){

        return $this->hasOne('App\CompanyComment');
    }

    /*************
     * Methods
     *************/

    public static function getStudents(){

        return User::all()->where('role_id', 1);
    }

    public static function getTeachers(){

        return User::all()->where('role_id', 2);
    }

}
