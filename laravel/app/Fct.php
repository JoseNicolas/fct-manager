<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fct extends Model
{
    protected $table = 'fct';

    public function user(){

        return $this->hasOne('App\User');
    }

}
