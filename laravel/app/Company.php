<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'companies';

    protected $fillable = array(
        'id',
        'cif',
        'fiscal_name',
        'trade_name',
        'phone',
        'company_email',
        'address',
        'province',
        'locality',
        'cp',
        'role_id',
        'active',
        'id'
    );
}
