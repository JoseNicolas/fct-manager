/**
 * Created by josenicolas on 16/05/16.
 */

//function para desactivar usuarios del panel
function disable_user(id)
{
    //(titulo, mensaje, onok, oncancel)
    alertify.confirm("Desactivar Usuario:","¿Desea desactivar el usuario " + id + " ?", function (e) {$('#form' + id).submit();}, function(f){alertify.set('notifier', 'delay', 10);
        alertify.set('notifier', 'position', 'top-right');
        alertify.error('Operación cancelada');});
}
//Si existe mensaje de estado(almacenado en la variable session('status')) se muestra al usuario mediante una notificación
function show_notifier(){

    alertify.set('notifier','delay', 10);
    alertify.set('notifier','position', 'top-right');
    alertify.success("{{ session('status') }}");
}