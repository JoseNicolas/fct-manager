<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            // DATOS PERSONALES

            $table->increments('id');
            $table->string('dni', 9)->unique();
            $table->string('name', 100);
            $table->string('surnames', 100);
            $table->string('phone', 12);
            $table->string('address');
            $table->string('province', 100);
            $table->string('locality', 100);
            $table->string('cp', 6);
            $table->string('email');
            $table->enum('gender', [
                'male',
                'female'
            ])->nullable();

            // DATOS CUENTA
            $table->string('username')->unique();
            $table->string('password');
            $table->integer('role_id')->unsigned();
            $table->tinyInteger('active')->default(0);
            $table->dateTime('last_access')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles');

//		$table->primary ( 'dni' ); // primary key
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
