<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFctTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fct', function (Blueprint $table) {

            // CAMPOS FCT
            $table->increments('id');
            $table->integer('student_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->date('start_date');
            $table->date('end_date');

            $table->integer('tutor_id')->unsigned();
            $table->string('instructor', 200);
            $table->integer('fct_status')->unsigned();//Estado en el que se encuentra la fct
            $table->integer('total_hours', false, true);
            $table->integer('hours_worked', false, true);

            //DB::statement('ALTER TABLE fcts DROP PRIMARY KEY , ADD PRIMARY KEY ( `id` , `alumno` , `empresa` , `fecha_inicio` )');

            $table->foreign('student_id')->references('id')->on('users');
            $table->foreign('tutor_id')->references('id')->on('users');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('fct_status')->references('id')->on('fct_status');
            // Foreign keys
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fct');
    }
}