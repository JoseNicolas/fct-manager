<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_comments', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('author_id')->unsigned();
            $table->integer('reciver_id')->unsigned();
            $table->text('content');
            $table->timestamps();

            //DB::statement('ALTER TABLE comentarios DROP PRIMARY KEY , ADD PRIMARY KEY ( `id` , `autor` , `destinatario` , `fecha` )');

            $table->foreign('author_id')->references('id')->on('users'); // Foreign keys
            $table->foreign('reciver_id')->references('id')->on('users'); // Foreign keys
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('student_comments');
    }
}
