<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {

            // DATOS EMPRESA
            $table->increments('id');
            $table->string('cif', 9)->unique();
            $table->string('fiscal_name', 100);
            $table->string('trade_name', 100);
            $table->string('address', 200);
            $table->string('phone', 12);
            $table->string('province', 100);
            $table->string('locality', 100);
            $table->string('company_email')->unique();
            $table->string('cp', 6);
            $table->string('latitude')->nullable(); // Localizacion, maps
            $table->string('longitude')->nullable();

            // DATOS GERENCIA
            $table->string('manager')->nullable();//Gerente
            $table->string('manager_dni', 9)->nullable();
            $table->string('manager_email')->nullable();

            $table->string('representative')->nullable();//Representante, subgerente
            $table->string('representative_dni', 9)->nullable();
            $table->string('representative_email')->nullable();

            // OTROS DATOS
            $table->boolean('active')->default(false); // Empresa activa o no
            $table->timestamps(); // created_at, updated_at

//            $table->primary('cif');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}