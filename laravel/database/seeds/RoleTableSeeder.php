<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker::create();

        \DB::table ( 'roles' )->insert (array(
            'id' => 1,
            'role_name'     => 'student',
            'created_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
        ));

        \DB::table ( 'roles' )->insert (array(
            'id' => 2,
            'role_name'     => 'teacher',
            'created_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
        ));

        \DB::table ( 'roles' )->insert (array(
            'id' => 3,
            'role_name'     => 'company',
            'created_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
        ));

        \DB::table ( 'roles' )->insert (array(
            'id' => 0,
            'role_name'     => 'admin',
            'created_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at'    => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
        ));
    }
}
