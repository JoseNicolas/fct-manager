<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i = 0; $i < 20; $i++) {

            \DB::table('companies')->insert(array(

                'cif' => $faker->randomNumber($nbDigits = 9),
                'fiscal_name' => $faker->company,
                'trade_name' => $faker->company,
                'address' => $faker->streetAddress,
                'phone' => $faker->randomNumber($nbDigits = 9),
                'province' => $faker->state,
                'locality' => $faker->city,
                'company_email' => $faker->email,
                'cp' => $faker->postcode,
                'manager' => $faker->firstName,//Gerente
                'manager_dni' => $faker->randomNumber($nbDigits = 9),
                'manager_email' => $faker->email,
                'active' => $faker->randomElement([
                    0,
                    1
                ])
            ));
        }
    }
}
