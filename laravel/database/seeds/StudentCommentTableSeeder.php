<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class StudentCommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create ();

//        $alumnos = DB::select ( 'select * from users where role = :role', [
//            'role' => 1
//        ] );

        $alumno = App\User::getStudents()->first();

//        $profesores = DB::select ( 'select * from users where role = :role', [
//            'role' => 2
//        ] );

        $profesores = App\User::getTeachers();

//        for($i = 0; $i < 10; $i ++) {
//
//            \DB::table ( 'student_comments' )->insert ( array (
//
//                'author_id' =>$profesores[0]->id,
//                'reciver_id' => $alumnos[2]->id,
//                'content' => $faker->text,
//                'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
//                'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
//
//            ) );
//
//        }

        foreach($profesores as $profesor){

            \DB::table ( 'student_comments' )->insert ( array (

                'author_id' =>$profesor->id,
                'reciver_id' => $alumno->id,
                'content' => $faker->text,
                'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

            ) );
        }

//        for($i = 0; $i < 10; $i ++) {
//
//            \DB::table ( 'student_comments' )->insert ( array (
//
//                'author_id' =>$profesores[2]->id,
//                'reciver_id' => $alumnos[1]->id,
//                'content' => $faker->text,
//                'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
//                'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
//
//            ) );
//
//        }

    }
}
