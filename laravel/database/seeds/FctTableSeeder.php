<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class FctTableSeeder extends Seeder {
/**
 * Run the database seeds.
 *
 * @return void
 */
public function run() {
	$faker = Faker::create ();

	$fks = new \Faker\ORM\Propel\Populator ( $faker );

	$alumnos = DB::select ( 'select * from users where role_id = :role_id', [
			'role_id' => 1
	] );

	$empresas = DB::select ( 'select * from companies' );

	$profesores = DB::select ( 'select * from users where role_id = :role_id', [
			'role_id' => 2
	] );
	
	foreach ( $alumnos as $alumno ) {
		
		\DB::table ( 'fct' )->insert ( array (
				'student_id' => $alumno->id,
				'company_id' => $empresas[0]->id,
				'start_date' => $faker->date,
				'end_date' => $faker->date,
				'tutor_id' => $profesores[0]->id,
				'instructor' => $faker->firstName,
				'fct_status' => $faker->randomElement (array(1, 2, 3, 4, 5)),
				'total_hours' => $faker->numberBetween ( $min = 350, $max = 400 ),
				'hours_worked' => $faker->numberBetween ( $min = 0, $max = 400 )
		) );
	}
}
}
