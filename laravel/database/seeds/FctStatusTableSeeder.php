<?php

use Illuminate\Database\Seeder;

class FctStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table ( 'fct_status' )->insert ( array (
            'id' => 1,
            'status_name' => 'coursing',
            'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

        ) );

        \DB::table ( 'fct_status' )->insert ( array (
            'id' => 2,
            'status_name' => 'finalized',
            'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

        ) );

        \DB::table ( 'fct_status' )->insert ( array (
            'id' => 3,
            'status_name' => 'not finalized',
            'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

        ) );

        \DB::table ( 'fct_status' )->insert ( array (
            'id' => 4,
            'status_name' => 'waiting',
            'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

        ) );

        \DB::table ( 'fct_status' )->insert ( array (
            'id' => 5,
            'status_name' => 'others',
            'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

        ) );

    }
}
