<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompanyCommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create ();


//        $empresas = DB::select ( 'select * from companies' );
        $empresas = App\Company::all();

//        $profesores = DB::select ( 'select * from users where role = :role', [
//            'role' => 2
//        ] );

        $profesor = App\User::getTeachers()->first();

        foreach ($empresas as $empresa) {

            \DB::table ( 'company_comments' )->insert ( array (

                'author_id' =>$profesor->id,
                'reciver_id' => $empresa->id,
                'content' => $faker->text,
                'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

            ) );
        }

//        for($i = 0; $i < 10; $i ++) {
//
//            \DB::table ( 'company_comments' )->insert ( array (
//
//                'author_id' =>$profesores[0]->id,
//                'reciver_id' => $empresas[0]->id,
//                'content' => $faker->text,
//                'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
//                'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
//
//            ) );
//
//        }
//        for($i = 0; $i < 10; $i ++) {
//
//            \DB::table ( 'company_comments' )->insert ( array (
//
//                'author_id' =>$profesores[1]->id,
//                'reciver_id' => $empresas[2]->id,
//                'content' => $faker->text,
//                'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
//                'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()
//
//            ) );
//
//        }

    }
}
