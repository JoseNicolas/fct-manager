<?php
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = Faker::create ();

		$rol = array(1, 2, 3);

		for($i = 0; $i < 20; $i ++) {

			\DB::table ( 'users' )->insert ( array (

					'dni' => $dni = $faker->randomNumber ( $nbDigits = 9 ),
					'name' => $faker->firstName,
					'surnames' => $faker->lastName,
					'phone' => $faker->randomNumber ( $nbDigits = 9 ),
					'address' => $faker->streetAddress,
					'province' => $faker->state,
					'locality' => $faker->city,
					'cp' => $faker->postcode,
					'email' => $faker->email,
					'gender' => $faker->randomElement ( [
							'male',
							'female'
					] ),
					'username' => $username = $faker->userName,
					'password' => bcrypt($username),
					'role_id' => $faker->randomElement ( $rol ),
					'active' => $faker->randomElement ( [
							0,
							1
					] ),
					'last_access' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
                    'created_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::createFromDate(2014,07,22)->toDateTimeString()

			) );

		}
	}
}
