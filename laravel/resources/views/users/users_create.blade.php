@extends('layouts.dashboard')
@section('title', 'Crear Usuario')
@section('breadcrumbs', Breadcrumbs::render('users.create'))
@section('content')
    <div class="container">
        <h1>Nuevo Usuario</h1>
        <hr>
        <div class="row">
            <!-- left column -->
            <div class="col-md-3">
                <h3>Imágen Actual</h3>
                <div class="text-center">
                    <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
                    <h6>Cambie su foto de perfil...</h6>

                    <input type="file" class="form-control">
                </div>
            </div>

            <!-- edit form column -->
            <div class="col-md-9 personal-info">

                <h3>Información Personal</h3>

                <form class="form-horizontal" role="form" method="POST" action="/users">

                    <div class="form-group">
                        <label class="col-lg-3 control-label">Nombre:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Apellidos:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="surnames">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Direccion:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Teléfono:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">DNI:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="dni">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Género:</label>
                        <div class="col-lg-8">
                            <div class="ui-select">
                                <select name="gender" class="form-control">
                                    <option value="male" selected="selected">Hombre</option>
                                    <option value="female">Mujer</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Provincia:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="province">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Localidad:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="locality">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">C.P.:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="cp">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">e-Mail:</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Username:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="text" name="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Password:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password" name="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirm password:</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password" name="repeat_password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-primary" value="Enviar">
                        </div>
                    </div>

                    {{ csrf_field() }}

                </form>
            </div>
        </div>
    </div>
    <hr>

@endsection