@extends('layouts.dashboard')
@section('title', 'Usuarios')
@section('breadcrumbs', Breadcrumbs::render('users.index'))
@section('js','<script src="/js/users/users_index.js"></script>')
@section('content')

<<<<<<< HEAD
    {{--Si existe mensaje de estado se muestra al usuario mediante una notificación--}}
    @if(session('status'))
        <script>
            show_notifier()
=======
    <script>
        function disable_user(id)
        {
            alertify.confirm("¿Desea eliminar usuario " + id + " ?", function (e) {
                if (e) {
                    $('#form' + id).submit();
                } else {
                    alertify.set('notifier', 'delay', 10);
                    alertify.set('notifier', 'position', 'top-right');
                    alertify.error('Operación cancelada');
                }
            });
        }
    </script>

    @if(session('status'))
        <script>
            alertify.set('notifier','delay', 10);
            alertify.set('notifier','position', 'top-right');
            alertify.success('{{ session('status') }}');
>>>>>>> 147e555229528e72aaadda173cab02f8136cdcb1
        </script>
    @endif

    <div class="container-fluid" style="padding-left: 0;padding-right: 0">
        <table id="datatable" class="hover" cellspacing="0" width="80%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Surnames</th>
                    <th>Telephone</th>
                    <th>Address</th>
                    <th>Province</th>
                    <th>Locality</th>
                    <th>CP</th>
                    <th>Email</th>
                    <th>Role</th>
                    {{--<th>Active</th>--}}
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                @foreach($users as $user)
                    <?php $i = 1; ?>
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->surnames }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>{{ $user->address }}</td>
                        <td>{{ $user->province }}</td>
                        <td>{{ $user->locality }}</td>
                        <td>{{ $user->cp }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->role->role_name }}</td>
                        {{--<td>{{ $user->active }}</td>--}}
<<<<<<< HEAD
                        <td><button onclick="location.href = '{{url('/users/'.$user->id.'/edit')}}'" href="{{url('/users/'.$user->id.'/edit')}}" class="glyphicon glyphicon-pencil btn" style="color: #30a5ff"></button>
                        {{ Form::open(array('url' => 'users/' . $user->id, 'class' => 'pull-right', 'onclick' => 'disable_user('.$user->id.')', 'id' => 'form'.$user->id)) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::button('', array('type' => 'button','class' => 'glyphicon glyphicon-trash btn', 'style' => 'float:left; color: #30a5ff;')) }}
=======
                        <td><button onclick="location.href = '{{url('/users/'.$user->id.'/edit')}}'" href="{{url('/users/'.$user->id.'/edit')}}" class="glyphicon glyphicon-pencil" style="color: #30a5ff"></button>
                        {{ Form::open(array('url' => 'users/' . $user->id, 'class' => 'pull-right', 'onclick' => 'disable_user('.$user->id.')', 'id' => 'form'.$user->id)) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::button('', array('type' => 'button','class' => 'glyphicon glyphicon-trash', 'style' => 'float:left; color: #30a5ff;')) }}
>>>>>>> 147e555229528e72aaadda173cab02f8136cdcb1
                        {{ Form::close() }}</td>

                    </tr>

                    <?php $i++; ?>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
