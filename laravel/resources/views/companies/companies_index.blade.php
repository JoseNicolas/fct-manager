@extends('layouts.dashboard')
@section('title', 'Usuarios')
@section('content')

    <script>
        function disable_user(id)
        {
            alertify.confirm("¿Desea eliminar usuario " + id + " ?", function (e) {
                if (e) {
                    $('#form' + id).submit();
                } else {
                    alertify.set('notifier', 'delay', 10);
                    alertify.set('notifier', 'position', 'top-right');
                    alertify.error('Operación cancelada');
                }
            });
        }
    </script>

    @if(session('status'))
        <script>
            alertify.set('notifier','delay', 10);
            alertify.set('notifier','position', 'top-right');
            alertify.success('{{ session('status') }}');
        </script>
    @endif

    <div class="container-fluid" style="padding-left: 0;padding-right: 0">
        <table id="datatable" class="hover" cellspacing="0" width="80%">
            <thead>
            <tr>
                <th>Id</th>
                <th>CIF</th>
                <th>Nombre</th>
                <th>Address</th>
                <th>Telephone</th>
                <th>Province</th>
                <th>Locality</th>
                <th>CP</th>
                <th>Email</th>
                {{--<th>Role</th>--}}
                <th>Active</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($companies as $company)
                <?php $i = 1; ?>
                <tr>
                    <td>{{ $company->id }}</td>
                    <td>{{ $company->cif }}</td>
                    <td>{{ $company->trade_name }}</td>
                    <td>{{ $company->address }}</td>
                    <td>{{ $company->phone }}</td>
                    <td>{{ $company->province }}</td>
                    <td>{{ $company->locality }}</td>
                    <td>{{ $company->cp }}</td>
                    <td>{{ $company->company_email }}</td>
{{--                    <td>{{ $company->role->role_name }}</td>--}}
                    <td>{{ $company->active }}</td>
                    <td><button onclick="location.href = '{{url('/companies/'.$company->id.'/edit')}}'" href="{{url('/companies/'.$company->id.'/edit')}}" class="glyphicon glyphicon-pencil btn" style="color: #30a5ff"></button>
                        {{ Form::open(array('url' => 'companies/' . $company->id, 'class' => 'pull-right', 'onclick' => 'disable_user('.$company->id.')', 'id' => 'form'.$company->id)) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::button('', array('type' => 'button','class' => 'glyphicon glyphicon-trash btn', 'style' => 'float:left; color: #30a5ff;')) }}
                        {{ Form::close() }}</td>

                </tr>

                <?php $i++; ?>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
