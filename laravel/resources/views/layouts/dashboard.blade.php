<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>FCT Manager - @yield('title')</title>

<link href="/img/home/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />

<<<<<<< HEAD
{{-- BootStrap CSS --}}
<link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
=======
{{-- CSS --}}
<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">
>>>>>>> 147e555229528e72aaadda173cab02f8136cdcb1
{{--<link href="css/datepicker3.css" rel="stylesheet">--}}
<link href="/css/dashboard/styles.css" rel="stylesheet">
<!-- Datatables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/t/dt/dt-1.10.11,r-2.0.2/datatables.min.css"/>
<!-- MyCSS-->
<<<<<<< HEAD
<link rel="stylesheet" href="/css/dashboard/dashboard_styles.css">
<!-- AlertifyJS-->
<link rel="stylesheet" href="/vendor/alertifyjs/css/alertify.min.css" />
@yield('css')

<!--JavaScript-->
<script src="/vendor/jquery/jquery-2.2.3.min.js"></script>
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/t/dt/dt-1.10.11,r-2.0.2/datatables.min.js"></script>
<script src="/vendor/alertifyjs/alertify.min.js"></script>
@yield('js')
=======
<link rel="stylesheet" href="/css/dashboard/styles.css">
<!-- AlertifyJS-->
<link rel="stylesheet" href="vendor/alertifyjs/css/alertify.min.css" />

<!--JavaScript-->
<script src="/jquery/jquery-2.2.3.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script src="/vendor/alertifyjs/alertify.min.js"></script>
>>>>>>> 147e555229528e72aaadda173cab02f8136cdcb1

{{--<!--[if lt IE 9]>--}}
{{--<script src="js/html5shiv.js"></script>--}}
{{--<script src="js/respond.min.js"></script>--}}
{{--<![endif]-->--}}

</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{url('/')}}"><span>FCT</span>Manager</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"><use xlink:href="#stroked-male-user"></use></span> {{Auth::user()->name}}<small> - {{Auth::user()->role->role_name}}</small> <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ url('/users' . Auth::user()->id . '/edit') }}"><span class="glyphicon glyphicon-user"><use xlink:href="#stroked-male-user"></use></span> Perfil</a></li>
							<li><a href="#"><span class="glyphicon glyphicon-cog"><use xlink:href="#stroked-gear"></use></span> Settings</a></li>
							<li><a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-circle-arrow-left"><use xlink:href="#stroked-cancel"></use></span> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
							
		</div><!-- /.container-fluid -->
	</nav>
		
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
			<div class="form-group">
				<input type="text" class="form-control" placeholder="Search">
			</div>
		</form>
		<ul class="nav menu">
			<li class="parent">
				<a href="#">
					<span data-toggle="collapse" href="#sub-usuarios"><span class="glyphicon glyphicon-user"><use xlink:href="#stroked-dashboard-dial"></use></span> Usuarios</span></a></li></span>
					<ul class="children collapse" id="sub-usuarios">
						<li>
							<a class="" href="{{url('/users')}}">
								<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Listar Usuarios</span>
							</a>
						</li>
						<li>
							<a class="" href="{{url('/users/create')}}">
								<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Nuevo Usuario</span>
							</a>
						</li>
					</ul>
			</li>
			<li class="parent">
				<a href="#">
					<span data-toggle="collapse" href="#sub-empresas"><span class="glyphicon glyphicon-briefcase"><use xlink:href="#stroked-dashboard-dial"></use></span> Empresas</span></a></li></span>
			<ul class="children collapse" id="sub-empresas">
				<li>
					<a class="" href="{{ url('/companies') }}">
						<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Listar Empresas</span>
					</a>
				</li>
				<li>
					<a class="" href="{{ url('/companies/create') }}">
						<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Nueva Empresa</span>
					</a>
				</li>
			</ul>
			</li>
			<li class="parent">
				<a href="#">
					<span data-toggle="collapse" href="#sub-comentarios"><span class="glyphicon glyphicon-comment"><use xlink:href="#stroked-dashboard-dial"></use></span> Comentarios</span></a></li></span>
			<ul class="children collapse" id="sub-comentarios">
				<li>
					<a class="" href="#">
						<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Ver Comentarios</span>
					</a>
				</li>
				<li>
					<a class="" href="#">
						<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Nuevo Comentario</span>
					</a>
				</li>
			</ul>
			</li>
			<li class="parent">
				<a href="#">
					<span data-toggle="collapse" href="#sub-fct"><span class="glyphicon glyphicon-retweet"><use xlink:href="#stroked-dashboard-dial"></use></span> FCTs</span></a></li></span>
			<ul class="children collapse" id="sub-fct">
				<li>
					<a class="" href="#">
						<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Ver FCTs</span>
					</a>
				</li>
				<li>
					<a class="" href="#">
						<span class="glyphicon glyphicon-menu-right"><use xlink:href="#stroked-chevron-right"></use></span> Nueva FCT</span>
					</a>
				</li>
			</ul>
			</li>

			{{--<li><a href="forms.html"><svg class="glyph stroked pencil"><use xlink:href="#stroked-pencil"></use></svg> Forms</a></li>--}}
			{{--<li><a href="panels.html"><svg class="glyph stroked app-window"><use xlink:href="#stroked-app-window"></use></svg> Alerts &amp; Panels</a></li>--}}
			{{--<li><a href="icons.html"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Icons</a></li>--}}
			{{--<li class="parent ">--}}
				{{--<a href="#">--}}
					{{--<span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg> Dropdown</span>--}}
				{{--</a>--}}
				{{--<ul class="children collapse" id="sub-item-1">--}}
					{{--<li>--}}
						{{--<a class="" href="#">--}}
							{{--<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 1--}}
						{{--</a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a class="" href="#">--}}
							{{--<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 2--}}
						{{--</a>--}}
					{{--</li>--}}
					{{--<li>--}}
						{{--<a class="" href="#">--}}
							{{--<svg class="glyph stroked chevron-right"><use xlink:href="#stroked-chevron-right"></use></svg> Sub Item 3--}}
						{{--</a>--}}
					{{--</li>--}}
				{{--</ul>--}}
			{{--</li>--}}
			<li role="presentation" class="divider"></li>
			<li><a href="{{ url('/logout') }}"><span class="glyphicon glyphicon-circle-arrow-left"><use xlink:href="#stroked-male-user"></use></span> Logout</a></li></span>
		</ul>

	</div><!--/.sidebar-->
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				{{--<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>--}}
				<li class="active"> @yield('breadcrumbs')</li>
			</ol>
		</div><!--/.row-->
		<br>
		
		@yield('content')

		<br>
	</div>	<!--/.main-->


<<<<<<< HEAD
=======
	{{--<script src="js/chart.min.js"></script>--}}
	{{--<script src="js/chart-data.js"></script>--}}
	{{--<script src="js/easypiechart.js"></script>--}}
	{{--<script src="js/easypiechart-data.js"></script>--}}
	{{--<script src="js/bootstrap-datepicker.js"></script>--}}
>>>>>>> 147e555229528e72aaadda173cab02f8136cdcb1
	<script>
		$(document).ready(function(){
			$('#datatable').DataTable({
						responsive: true
					});
		});
	</script>
	<script>
//		$('#calendar').datepicker({
//		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
</body>

</html>
