-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-06-2016 a las 00:58:11
-- Versión del servidor: 5.6.26-cll-lve
-- Versión de PHP: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `lg1t5nm1_lv52`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclos`
--

CREATE TABLE IF NOT EXISTS `ciclos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ciclo_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ciclo_grade` enum('Medio','Superior') COLLATE utf8_unicode_ci NOT NULL,
  `ciclo_acronym` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ciclo_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `ciclos`
--

INSERT INTO `ciclos` (`id`, `ciclo_name`, `ciclo_grade`, `ciclo_acronym`, `ciclo_description`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Desarrollo de Aplicaciones Web', 'Superior', 'DAW', 'Ciclo formativo orientado en el desarrollo de aplicaciones WEB', 1, '2016-06-21 16:06:57', '2016-06-26 22:44:53'),
(2, 'Desarrollo de Aplicaciones Multiplataforma', 'Superior', 'DAM', 'Ciclo formativo orientado en el desarrollo de aplicaciones Multiplataforma', 1, '2016-06-21 16:06:57', '2016-06-26 22:44:54'),
(3, 'Administración y Finanzas', 'Superior', 'AYF', 'Ciclo formativo orientado a las finanzas', 1, '2016-06-21 16:06:57', '2016-06-26 22:44:54'),
(4, 'Marketing y Publicidad', 'Superior', 'MYP', 'Ciclo formativo orientado al marketing y la publicidad', 1, '2016-06-21 16:06:57', '2016-06-26 22:44:55'),
(5, 'Administración de Sistemas Informáticos en Red', 'Superior', 'ASIR', 'Ciclo formativo orientado a redes informáticas', 1, '2016-06-21 16:06:58', '2016-06-26 22:44:55'),
(6, 'Gestión Administrativa', 'Medio', 'GA', 'Ciclo formativo a la administración', 1, '2016-06-21 16:06:58', '2016-06-26 22:44:56'),
(7, 'Actividades Comerciales', 'Medio', 'AC', 'Ciclo formativo a las actividades comerciales', 1, '2016-06-21 16:06:58', '2016-06-26 22:44:56'),
(8, 'Sistemas Informáticos y Redes', 'Medio', 'SIR', 'Ciclo formativo a la gestión de redes informáticas', 1, '2016-06-21 16:06:58', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(10) unsigned NOT NULL,
  `comment_type` enum('Alumno','Empresa','Fct') COLLATE utf8_unicode_ci NOT NULL,
  `receiver_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `comments_author_id_foreign` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id`, `author_id`, `comment_type`, `receiver_id`, `title`, `content`, `active`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, 3, 'Alumno', 2, 'Valoracion alumno', 'Daniel es un alumno excelente!', 1, '2016-06-22 19:20:50', '2016-06-22 19:20:50', 0),
(2, 4, 'Fct', 3, 'Acerca de la valoración de la empresa', 'Segun el alumno la empresa le ha dado un trato exquisito.', 1, '2016-06-22 19:23:05', '2016-06-22 19:23:05', 0),
(3, 3, 'Fct', 2, 'Valoración FCT', 'No han dado buenas condiciones de trabajo al alumno. Muy cortos en cuanto a material.', 1, '2016-06-22 19:24:27', '2016-06-22 19:24:27', 0),
(4, 3, 'Fct', 2, 'Valoracion de la empresa en la FCT sobre el alumno', 'La empresa expone que el alumno es poco eficiente, debido ha esto no consideran contratarlo.', 1, '2016-06-22 19:25:33', '2016-06-22 19:25:33', 0),
(5, 12, 'Empresa', 5, 'Valoracion sobre la empresa', 'Muy buena empresa con muy buenas condiciones y excelente trato al alumnado. El gerente de la empresa comentó que iba a hacer una solicitud de mucho alumnado en los proximos cursos.', 1, '2016-06-22 19:29:08', '2016-06-22 19:29:08', 0),
(6, 12, 'Alumno', 2, 'Acerca de la aptitud del alumno', 'Tuvo ciertos problemas familiares que le llevaron a dejar su ultima FCT.', 1, '2016-06-22 19:30:33', '2016-06-26 22:51:28', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cif` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `concierto` int(11) NOT NULL,
  `fiscal_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `trade_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `locality` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `company_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cp` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_dni` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `manager_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `representative` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `representative_dni` varchar(9) COLLATE utf8_unicode_ci DEFAULT NULL,
  `representative_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `overall_evaluation` int(11) NOT NULL DEFAULT '0',
  `without_asking_students` int(11) NOT NULL DEFAULT '0',
  `edited_by` tinyint(4) NOT NULL DEFAULT '0',
  `requests_without_students` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `companies_cif_unique` (`cif`),
  UNIQUE KEY `companies_company_email_unique` (`company_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `cif`, `concierto`, `fiscal_name`, `trade_name`, `address`, `phone`, `province`, `locality`, `company_email`, `cp`, `latitude`, `longitude`, `manager`, `manager_dni`, `manager_email`, `representative`, `representative_dni`, `representative_email`, `active`, `overall_evaluation`, `without_asking_students`, `edited_by`, `requests_without_students`, `created_at`, `updated_at`) VALUES
(1, 'B53622381', 53622381, 'energy control sl', 'Energy Control S.L.', 'C/Valverde del camino, 10', '966366363', 'Alicante', 'Elche', 'info@energycontrol.es', '03206', '38.2839714', '-0.718618', 'Manuel Lopez Serna', '11111111A', 'manuel@energycontrol.es', 'Francisco Quiles Palazon', '11111111A', 'josevicente@energycontrol.es', 1, 1, 0, 1, 0, '2016-06-26 22:06:43', '2016-06-26 22:06:43'),
(2, 'B21966105', 21966105, 'tic Levante s.l.', 'TIC Levante S.L.', 'C/Pascual Quiles, 23', '966366363', 'Alicante', 'Elche', 'info@ticlevante.es', '03206', '38.2649137', '-0.7104401', 'Arturo Fernandez Placio', '11111111A', 'arturo@gmail.es', 'Eugenio Rocamora Sanchez', '11111111A', 'eugenio@gmail.es', 1, 8, 0, 1, 0, '2016-06-26 22:06:10', '2016-06-26 22:06:10'),
(3, 'B53622388', 53622388, 'everis consultora s.l.', 'Everis Spain S.L.', 'Av. Oscar Espla, 37', '966366363', 'Alicante', 'Alicante', 'everis@everis.es', '03330', '38.3427679', '-0.4947927', 'Arturo Fernandez Placio', '11111111A', 'arturo@gmail.es', 'Eugenio Rocamora Sanchez', '11111111A', 'eugenio@gmail.es', 1, 4, 0, 1, 0, '2016-06-26 22:06:21', '2016-06-26 22:06:21'),
(4, 'B21966100', 21966100, 'query informatica s.l.', 'Query Informatica S.L.', 'Calle Espronceda, 113', '966366363', 'Alicante', 'Elche', 'query@query.es', '03206', '38.2577266', '-0.706407', 'Arturo Fernandez Placio', '11111111A', 'arturo@gmail.es', 'Eugenio Rocamora Sanchez', '11111111A', 'eugenio@gmail.es', 1, 0, 0, 1, 0, '2016-06-26 22:06:32', '2016-06-26 22:06:32'),
(5, 'B21966101', 21966101, 'clave informatica s.l.', 'Clave Informatica S.L.', 'Elche Parque Empresarial, Calle Galileo Galilei', '966366363', 'Alicante', 'Elche', 'clave@clave.es', '03206', '38.2878535', '-0.6118057', 'Arturo Fernandez Placio', '11111111A', 'arturo@gmail.es', 'Eugenio Rocamora Sanchez', '11111111A', 'eugenio@gmail.es', 1, 0, 0, 1, 0, '2016-06-26 22:06:44', '2016-06-26 22:06:44'),
(6, 'B31125601', 31125601, 'alto software s.l.', 'Alto Software, S.L.', 'Passeig de l''Esplanada, 7', '966366363', 'Alicante', 'Petrer', 'altosoftware@altosoftware.es', '02100', '38.4828491', '-0.7714201', 'Arturo Fernandez Placio', '11111111A', 'arturo@gmail.es', 'Eugenio Rocamora Sanchez', '11111111A', 'eugenio@gmail.es', 1, 0, 0, 1, 0, '2016-06-26 22:06:54', '2016-06-26 22:06:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company_statics`
--

CREATE TABLE IF NOT EXISTS `company_statics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course` int(11) NOT NULL,
  `students_requested` int(11) NOT NULL,
  `students_requested_date` datetime DEFAULT NULL,
  `assigned_students` int(11) DEFAULT NULL,
  `tutor_assessment` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `contract_probability` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `requesting_user_id` int(11) NOT NULL,
  `daw` int(11) NOT NULL DEFAULT '0',
  `dam` int(11) NOT NULL DEFAULT '0',
  `ayf` int(11) NOT NULL DEFAULT '0',
  `myp` int(11) NOT NULL DEFAULT '0',
  `asir` int(11) NOT NULL DEFAULT '0',
  `ga` int(11) NOT NULL DEFAULT '0',
  `ac` int(11) NOT NULL DEFAULT '0',
  `sir` int(11) NOT NULL DEFAULT '0',
  `check` tinyint(4) NOT NULL DEFAULT '0',
  `formalized` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `company_statics`
--

INSERT INTO `company_statics` (`id`, `course`, `students_requested`, `students_requested_date`, `assigned_students`, `tutor_assessment`, `tutor_id`, `contract_probability`, `company_id`, `requesting_user_id`, `daw`, `dam`, `ayf`, `myp`, `asir`, `ga`, `ac`, `sir`, `check`, `formalized`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, 2015, 0, '0000-00-00 00:00:00', 0, 5, 3, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '2016-06-22 16:06:00', '2016-06-22 22:24:31', 0),
(2, 2015, 0, '0000-00-00 00:00:00', 0, 8, 3, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '2016-06-22 16:06:29', '2016-06-22 22:25:35', 0),
(3, 2015, 0, '0000-00-00 00:00:00', 0, 4, 4, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '2016-06-22 17:06:09', '2016-06-22 22:27:16', 0),
(4, 2015, 0, '0000-00-00 00:00:00', 0, 0, 4, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '2016-06-22 17:06:10', '2016-06-22 22:19:47', 0),
(5, 2015, 0, '0000-00-00 00:00:00', 0, 0, 12, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '2016-06-22 17:06:16', '2016-06-22 22:19:19', 0),
(6, 2015, 0, '0000-00-00 00:00:00', 0, 0, 3, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, '2016-06-22 18:06:52', '2016-06-22 22:18:22', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fcts`
--

CREATE TABLE IF NOT EXISTS `fcts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `ciclo_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `tutor_id` int(10) unsigned NOT NULL,
  `instructor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instructor_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fct_status` int(10) unsigned NOT NULL,
  `total_hours` int(10) unsigned NOT NULL,
  `hours_worked` int(10) unsigned NOT NULL,
  `offer` tinyint(4) NOT NULL DEFAULT '0',
  `contract` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fcts_student_id_foreign` (`student_id`),
  KEY `fcts_tutor_id_foreign` (`tutor_id`),
  KEY `fcts_company_id_foreign` (`company_id`),
  KEY `fcts_fct_status_foreign` (`fct_status`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `fcts`
--

INSERT INTO `fcts` (`id`, `student_id`, `company_id`, `ciclo_id`, `start_date`, `end_date`, `tutor_id`, `instructor`, `instructor_email`, `fct_status`, `total_hours`, `hours_worked`, `offer`, `contract`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, 10, 3, 1, '2016-03-07', '2016-05-27', 3, 'Carmen Maria Piqueras', 'carmenpiqueras@gmail.com', 2, 400, 0, 0, 0, '2016-06-22 18:50:31', '2016-06-22 18:52:11', 0),
(2, 2, 3, 1, '2016-03-01', '2016-05-28', 3, 'Antonio Rodriguez', 'instructor@gmail.com', 2, 400, 0, 0, 1, '2016-06-22 18:51:45', '2016-06-22 18:52:23', 0),
(3, 7, 4, 3, '2016-03-06', '2016-06-08', 4, 'Antonio Jose Diaz', 'instructor@gmail.com', 4, 400, 400, 1, 1, '2016-06-22 18:54:15', '2016-06-22 18:54:15', 0),
(4, 11, 5, 2, '2016-03-16', '2016-06-01', 12, 'Tomas Haro Mendez', 'instructor@gmail.com', 4, 400, 400, 0, 0, '2016-06-22 18:55:17', '2016-06-22 18:55:17', 0),
(5, 2, 6, 1, '2016-03-08', '2016-05-27', 3, 'Jose Javier Planelles', 'instructor@gmail.com', 3, 400, 128, 0, 0, '2016-06-22 18:58:32', '2016-06-22 18:58:46', 0),
(6, 10, 2, 1, '2016-03-09', '2016-06-01', 3, 'David Murcia ', 'instructor@gmail.com', 4, 400, 400, 0, 0, '2016-06-22 18:59:49', '2016-06-22 19:00:02', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fct_status`
--

CREATE TABLE IF NOT EXISTS `fct_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `fct_status`
--

INSERT INTO `fct_status` (`id`, `status_name`, `created_at`, `updated_at`) VALUES
(1, 'Cursando', '2014-07-22 16:46:57', '2014-07-22 16:46:57'),
(2, 'Finalizada', '2014-07-22 16:46:57', '2014-07-22 16:46:57'),
(3, 'No finalizada', '2014-07-22 16:46:57', '2014-07-22 16:46:57'),
(4, 'A la espera', '2014-07-22 16:46:57', '2014-07-22 16:46:57'),
(5, 'Otros', '2014-07-22 16:46:57', '2014-07-22 16:46:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_04_21_193703_create_roles_table', 1),
('2014_10_11_113140_create_companies_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_04_15_195538_create_fct_status_table', 1),
('2016_04_16_133438_create_fcts_table', 1),
('2016_05_09_155059_create_users_companies_table', 1),
('2016_05_18_190842_create_comments_table', 1),
('2016_06_03_211525_create_company_statics_table', 1),
('2016_06_07_231419_create_ciclos_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'Alumno', '2014-07-22 16:46:57', '2014-07-22 16:46:57'),
(2, 'Profesor', '2014-07-22 16:46:57', '2014-07-22 16:46:57'),
(3, 'Empresa', '2014-07-22 16:46:57', '2014-07-22 16:46:57'),
(4, 'Admin', '2014-07-22 16:46:57', '2014-07-22 16:46:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dni` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surnames` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `locality` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cp` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` enum('Hombre','Mujer') COLLATE utf8_unicode_ci DEFAULT NULL,
  `birth_date` date NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `last_access` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `edited_by` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_dni_unique` (`dni`),
  UNIQUE KEY `users_username_unique` (`username`),
  KEY `users_role_id_foreign` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `dni`, `name`, `surnames`, `phone`, `address`, `province`, `locality`, `cp`, `email`, `gender`, `birth_date`, `username`, `password`, `role_id`, `active`, `last_access`, `remember_token`, `created_at`, `updated_at`, `edited_by`) VALUES
(1, '00000000X', 'Admin', 'Administrador', '000000000', 'Address', 'Province', 'Locality', '00000', 'admin@admin.com', 'Hombre', '1956-09-20', 'Admin', '$2y$10$lPzn0wQ0jnv3mNrGeWJFseVi82P0RvzhlFQEBzNUFQNq9/UPel33e', 4, 1, '0000-00-00 00:00:00', 'FvMbjZOixbP20LiHixatoUO6bRbIuzpZvJoj9hbM8woPCf1msKPdFBe6PUmV', NULL, '2016-06-25 01:28:04', 0),
(2, '21966103R', 'Daniel', ' Perez Martin', '966366363', 'C/Pascual Quiles, 23', 'Alicante', 'Elche', '03206', 'daniel@gmail.com', 'Hombre', '1993-02-10', 'daniel', '$2y$10$2bJItxKs0AM/eyA8UxgbOOA2NKWW8Xk9vzY7ja5AnndYMhVvxj/cm', 1, 1, NULL, '0QcxS0XMtPqjfOAwcZHSzHmpyJD7IWYaz8FQSUEHTce9gr0ydLdKH0Kxqlln', '2016-06-22 17:30:21', '2016-06-25 02:27:08', 0),
(3, '73065421R', 'Mario', ' Martin Rocamora', '966366363', 'C/Pascual Quiles, 23', 'Alicante', 'Elda', '03206', 'mario@yahoo.es', 'Hombre', '1988-06-18', 'mario', '$2y$10$orRJHWDfOdb0wEWVN2EeZ.pNE96HMOWagdqGNnWbcquDVsDLITeKG', 2, 1, NULL, 'ux7MLBaAzKrPoShDUOIKbfY6P1CcHqoQsq7z7yTFM9w8XPVHCD8efxAxaZrn', '2016-06-22 17:32:23', '2016-06-25 01:10:13', 0),
(4, '61966983R', 'Ana Maria', 'Muñoz Mestre', '966366363', 'C/Pascual Quiles, 23', 'Alicante', 'Alicante', '03206', 'anamario@info.es', 'Mujer', '1971-11-19', 'anamaria', '$2y$10$X.Oe7UMts989qqk.kCZo3.G9ntWvaDFyizQyLShtboxKdCgUknpta', 2, 1, NULL, 'PyNfE4gMJDMfgo5zVifvFaLYmJUsJB74e7KtC3NlMmd3S3BJwfjx7xqUX6Tf', '2016-06-22 17:33:33', '2016-06-22 19:23:13', 0),
(5, '23133257M', 'Pedro Jose', ' Cartagena Sanchez', '966366363', 'C/Pascual Quiles, 23', 'Alicante', 'Elche', '03206', 'pedrojose@gmail.com', 'Hombre', '1966-03-17', 'pedrojose', '$2y$10$LD4zk278wA5CZmXnni7REemn5jWWDpnat.y6jM4jLIlyDu3UqAGRq', 1, 1, NULL, NULL, '2016-06-22 17:35:14', '2016-06-22 18:34:56', 0),
(6, '12345678A', 'EneryControlUser', 'EnergyControlUser', '966366363', 'C/Valverde del camino, 10', 'Alicante', 'Elche', '03206', 'daniel@gmail.com', 'Hombre', '1986-02-19', 'energycontroluser', '$2y$10$yxlZxylJrS/wqRry3LJZCuWktO8FIQSMMLKA5sR40t7u24t6tN3be', 3, 1, NULL, 'TtRnK5uB4QW17K3XdVdbIN2K8cF2PN1iCugBvFqx2yfNlnJKCvZWjZY9YzPr', '2016-06-22 17:36:19', '2016-06-25 02:26:43', 0),
(7, 'X78546355', 'Nuria', 'Menarguez Sanchez', '966366363', 'Av. Oscar Espla, 37', 'Alicante', 'Alicante', '03330', 'nuria@yahoo.es', 'Hombre', '1990-06-13', 'nuria', '$2y$10$eWT8ia.ib/CcqpliRsCqAu9EFvhkMche.qpSNwNM9.5OnpuB4ZDXC', 1, 1, NULL, NULL, '2016-06-22 17:44:18', '2016-06-22 18:34:57', 0),
(8, '21966108R', 'Manuel AS', 'Iniesta Perez', '966366363', 'Passeig de l''Esplanada, 7', 'Alicante', 'Petrer', '03330', 'manuel@outlook.com', 'Hombre', '1983-10-20', 'manuelas', '$2y$10$uf7Iu8GBUEwBZC9FdqqTaeACGwfs.0iMO5mrnR9vMb9ciz/myDtEq', 3, 1, NULL, 'qIL9b367xHzEQ8rUVsjF61Y728lNidimyysVQokaLtbqRI0I5hZpdwwDlfEa', '2016-06-22 18:07:56', '2016-06-22 18:34:57', 0),
(9, '74375116P', 'Jose Francisco', 'Nicolas Bautista', '966366363', 'Calle Espronceda, 113', 'Alicante', 'Elche', '03206', 'josefrancisco@gmail.com', 'Hombre', '1990-06-02', 'josefrancisco', '$2y$10$8WxCVmPPN39zKFFQHjJ95.CTv4QEwaFuoA0DVLxn6Hw6q8vGaxDw6', 1, 0, NULL, 't6M50PlA643B5YIOZz4NLv6kRFoNzn5UBzmG1sFDy6ieyFCKtoXyFdoyA8zD', '2016-06-22 18:27:00', '2016-06-22 19:17:09', 0),
(10, '44065421R', 'Rafael', ' Mas Torregrosa', '966366363', 'C/Pascual Quiles, 23', 'Alicante', 'Elche', '03206', 'daniel@gmail.com', 'Hombre', '2016-06-02', 'rafael', '$2y$10$JNYVcnqhiS4CEq1kzVYZ2O1qq0NU78UekOyvaIaa2HO4PYYgqI7vO', 1, 1, NULL, NULL, '2016-06-22 18:27:46', '2016-06-22 18:34:58', 0),
(11, '36966103U', 'Monica', ' Martinez Sanz', '966366363', 'C/Pascual Quiles, 23', 'Alicante', 'Torrellano', '02100', 'daniel@gmail.com', 'Mujer', '2001-02-07', 'monica', '$2y$10$90QZfLBsSRA8B8/04aggm.s8tbcmnolOveg6f4gO.l.joPUFBpxom', 1, 1, NULL, NULL, '2016-06-22 18:28:46', '2016-06-22 18:35:00', 0),
(12, '21000328T', 'Pablo Jose', ' Ruiz Aranjuez', '966366363', 'C/Pascual Quiles, 23', 'Alicante', 'Elche', '03206', 'daniel@gmail.com', 'Hombre', '1978-08-18', 'pablojose', '$2y$10$ilFGPWu0UvLBq3RnMy2U7.BKM5Ot.eYP9lAGYhoCM2D7jmopUbjjO', 2, 1, NULL, 'tV0GaJIOpO5BOACf9NBvrSHYA6ff8QpOUEvy1oux0v8cZXTtRPXASqAaM3YB', '2016-06-22 18:30:15', '2016-06-23 11:16:45', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_companies`
--

CREATE TABLE IF NOT EXISTS `users_companies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `company_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_companies_user_id_foreign` (`user_id`),
  KEY `users_companies_company_id_foreign` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `users_companies`
--

INSERT INTO `users_companies` (`id`, `user_id`, `company_id`, `created_at`, `updated_at`) VALUES
(1, 6, 1, '2016-06-22 17:36:19', '2016-06-22 17:36:19'),
(2, 8, 6, '2016-06-22 18:07:56', '2016-06-22 18:07:56');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `fcts`
--
ALTER TABLE `fcts`
  ADD CONSTRAINT `fcts_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `fcts_fct_status_foreign` FOREIGN KEY (`fct_status`) REFERENCES `fct_status` (`id`),
  ADD CONSTRAINT `fcts_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fcts_tutor_id_foreign` FOREIGN KEY (`tutor_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `users_companies`
--
ALTER TABLE `users_companies`
  ADD CONSTRAINT `users_companies_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `companies` (`id`),
  ADD CONSTRAINT `users_companies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
